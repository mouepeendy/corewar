# Corewar
This project is a replica of the computer game Core War, launched in 1984.<br>
This project is composed of:
* an assembler to compile custom assembler code(Redcode) to bytecode.
* a VM to execute instructions in the game.

## The Game
The last player to stay alive wins!
runs better on 27" screen

## Build
To build the project you need to install ncurses

Build all<br>
`$ make`<br>
Build asm (Assembler)<br>
`$ make asm`<br>
Build corewar (VM)<br>
`$ make corewar`<br>

### Run
How to run ASM
```
$ ./asm champs/[champion_file].s
```
How to run VM
```
$ ./corewar [champion_file].cor ... [champion_file_4].cor
```
Corewar can handle 1 to 4 champions at a time.<br><br>
Inside the game:
* `spacebar: play or pause
* `e: increase execution speed by 1
* `w: decrease execution speed by 1
* `r: increase execution speed by 10
* `r: decrease execution speed by 10
* `d: debug view (process info + registers)
* `[left] or [right] arrow keys: switch between processes' debug view
